#!/bin/bash
set -e
##################################################################################################################
# Author	:	szorfein
# Theme     :   Battleship
# WM        :   awesomewm
# Website	:	https://github.com/szorfein/dotfiles
# Website	:	https://www.reddit.com/user/szorfein
# Website   :   https://www.reddit.com/r/unixporn/comments/c5bcq9/awesome_battleship/
##################################################################################################################
#repo
git clone https://github.com/szorfein/dotfiles
cd dotfiles
#stow -- set to what theme you want
stow theme-battleship   ##theme
stow awesomewm
stow config
stow images
#Stow 

#Remove anything you do not like from the installed packeges

