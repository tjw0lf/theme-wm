#!/bin/bash
set -e
##################################################################################################################
# Author	:	szorfein
# Theme     :   Battleship
# WM        :   awesomewm
# Website	:	https://github.com/szorfein/dotfiles
# Website	:	https://www.reddit.com/user/szorfein
# Website   :   https://www.reddit.com/r/unixporn/comments/c5bcq9/awesome_battleship/
##################################################################################################################

sudo pacman -Syyu --noconfirm
#installing awesomewm and packeges for szorfein-themes
sudo pacman -S --noconfirm --needed zsh git awesome stow termite compton xorg-xinit xorg-server rofi feh
sudo pacman -S --noconfirm --needed xf86-video-intel base-devel wget
#installing desktop environment
sudo pacman -S awesome --noconfirm --needed
#Sound
sudo pacman -S pulseaudio --noconfirm --needed
sudo pacman -S pulseaudio-alsa --noconfirm --needed
sudo pacman -S pavucontrol  --noconfirm --needed
sudo pacman -S alsa-utils alsa-plugins alsa-lib alsa-firmware --noconfirm --needed
sudo pacman -S gstreamer --noconfirm --needed
sudo pacman -S gst-plugins-good gst-plugins-bad gst-plugins-base gst-plugins-ugly --noconfirm --needed
sudo pacman -S volumeicon --noconfirm --needed
sudo pacman -S playerctl --noconfirm --needed
#Music players
sudo pacman -S mpd mpc ncmpcpp --noconfirm --needed
#Create a build directory
mkdir ~/build
#xst,snapshots and compile
cd ~/build
wget https://aur.archlinux.org/cgit/aur.git/snapshot/xst-git.tar.gz
tar xvf xst-git.tar.gz
cd xst-git
makepkg -si --noconfirm --needed
cd
#nerd-fonts-iosevka,snapshots and compile
cd ~/build
wget https://aur.archlinux.org/cgit/aur.git/snapshot/nerd-fonts-iosevka.tar.gz
tar xvf nerd-fonts-iosevka.tar.gz
cd nerd-fonts-iosevka
makepkg -si --noconfirm --needed
cd
#repo
git clone https://github.com/szorfein/dotfiles
cd dotfiles
#stow
stow theme-battleship  ### pick your theme
stow awesomewm
stow config
stow images
cd
#installing displaymanager or login manager
#sudo pacman -S --noconfirm --needed lightdm
#sudo pacman -S --noconfirm --needed lightdm-gtk-greeter lightdm-gtk-greeter-settings
#enabling displaymanager or login manager
#sudo systemctl enable lightdm.service -f
#sudo systemctl set-default graphical.target
#ZSH
cd dotfiles
cp zsh/.aliases.zsh $HOME
cp zsh/.dircolors $HOME
cp zsh/.zprofile $HOME
cp zsh/.zshenv $HOME
cp zsh/.zshrc $HOME
./install --zsh
#ZSH
#update mirrors again

sudo pacman -Syyu --noconfirm

#Remove anything you do not like from the installed packeges

#Comment out lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings if you do not wish to use