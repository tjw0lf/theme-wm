#!/bin/bash
set -e
##################################################################################################################
# Author	:	szorfein
# Theme     :   Battleship
# WM        :   awesomewm
# Website	:	https://github.com/szorfein/dotfiles
# Website	:	https://www.reddit.com/user/szorfein
# Website   :   https://www.reddit.com/r/unixporn/comments/c5bcq9/awesome_battleship/
##################################################################################################################
#repo
git clone https://github.com/szorfein/dotfiles
cd dotfiles
#stow -- set to what theme you want
stow -D theme-battleship   ## theme
stow -D awesomewm
stow -D config
stow -D images
#Stow Unlinking files  ## Uncomment comment when needed
#mkdir ~/battleship-unlinked  ##change directory to your needs -- backup location is
#moving files unlinked -- you can take out or put in whatever you want
#mv awesomewm images bin config theme-battleship ncmpcpp multimedia zsh ~/battleship-unlinked 

#Remove anything you do not like from the installed packeges

